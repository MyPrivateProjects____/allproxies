package com.allproxies.ProxyManager;

import static io.restassured.RestAssured.preemptive;
import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;
import static org.apache.commons.lang3.RandomStringUtils.randomNumeric;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import com.allproxies.ProxyManager.app.ProxyManagerApplication;
import com.allproxies.ProxyManager.model.Proxy;
import io.restassured.RestAssured;
import io.restassured.response.Response;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = { ProxyManagerApplication.class }, webEnvironment = WebEnvironment.DEFINED_PORT)
public class LiveTest {

    @Before
    public void setUp() {
        RestAssured.authentication = preemptive().basic("john", "123");
    }

    private static final String API_ROOT = "http://localhost:8081/api/proxys";

    @Test
    public void whenGetAllProxys_thenOK() {
        final Response response = RestAssured.get(API_ROOT);
        assertEquals(HttpStatus.OK.value(), response.getStatusCode());
    }

    @Test
    public void whenGetProxysByIp_thenOK() {
        final Proxy proxy = createRandomProxy();
        createProxyAsUri(proxy);

        final Response response = RestAssured.get(API_ROOT + "/ip/" + proxy.getIp());
        assertEquals(HttpStatus.OK.value(), response.getStatusCode());
        assertTrue(response.as(List.class)
                .size() > 0);
    }

    @Test
    public void whenGetCreatedProxyById_thenOK() {
        final Proxy proxy = createRandomProxy();
        final String location = createProxyAsUri(proxy);

        final Response response = RestAssured.get(location);
        assertEquals(HttpStatus.OK.value(), response.getStatusCode());
        assertEquals(proxy.getIp(), response.jsonPath()
                .get("ip"));
    }

    @Test
    public void whenGetNotExistProxyById_thenNotFound() {
        final Response response = RestAssured.get(API_ROOT + "/" + randomNumeric(4));
        assertEquals(HttpStatus.NOT_FOUND.value(), response.getStatusCode());
    }

    // POST
    @Test
    public void whenCreateNewProxy_thenCreated() {
        final Proxy proxy = createRandomProxy();

        final Response response = RestAssured.given()
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .body(proxy)
                .post(API_ROOT);
        assertEquals(HttpStatus.CREATED.value(), response.getStatusCode());
    }

    @Test
    public void whenInvalidProxy_thenError() {
        final Proxy proxy = createRandomProxy();
        proxy.setPort(null);

        final Response response = RestAssured.given()
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .body(proxy)
                .post(API_ROOT);
        assertEquals(HttpStatus.BAD_REQUEST.value(), response.getStatusCode());
    }

    @Test
    public void whenUpdateCreatedProxy_thenUpdated() {
        final Proxy proxy = createRandomProxy();
        final String location = createProxyAsUri(proxy);

        proxy.setId(Long.parseLong(location.split("api/proxys/")[1]));
        proxy.setPort("newPort");
        Response response = RestAssured.given()
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .body(proxy)
                .put(location);
        assertEquals(HttpStatus.OK.value(), response.getStatusCode());

        response = RestAssured.get(location);
        assertEquals(HttpStatus.OK.value(), response.getStatusCode());
        assertEquals("newPort", response.jsonPath()
                .get("port"));

    }

    @Test
    public void whenDeleteCreatedProxy_thenOk() {
        final Proxy proxy = createRandomProxy();
        final String location = createProxyAsUri(proxy);

        Response response = RestAssured.delete(location);
        assertEquals(HttpStatus.OK.value(), response.getStatusCode());

        response = RestAssured.get(location);
        assertEquals(HttpStatus.NOT_FOUND.value(), response.getStatusCode());
    }

    // ===============================

    private Proxy createRandomProxy() {
        final Proxy proxy = new Proxy();
        proxy.setIp(randomAlphabetic(10));
        proxy.setPort(randomAlphabetic(15));
        return proxy;
    }

    private String createProxyAsUri(Proxy proxy) {
        final Response response = RestAssured.given()
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .body(proxy)
                .post(API_ROOT);
        return API_ROOT + "/" + response.jsonPath()
                .get("id");
    }

}
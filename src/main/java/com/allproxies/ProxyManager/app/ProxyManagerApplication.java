package com.allproxies.ProxyManager.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@ComponentScan(basePackages = "com.allproxies.ProxyManager")
@EnableJpaRepositories(basePackages = "com.allproxies.ProxyManager.repository")
@EntityScan(basePackages = "com.allproxies.ProxyManager.model")
public class ProxyManagerApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProxyManagerApplication.class, args);
	}
}

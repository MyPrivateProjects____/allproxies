package com.allproxies.ProxyManager.repository;

import com.allproxies.ProxyManager.model.Proxy;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;

@RepositoryRestResource(collectionResourceRel = "proxies", path = "proxies")
public interface ProxyRepository extends CrudRepository<Proxy, Long> {//todo PagingAndSortingRepository

    List<Proxy> findByIsActive(@Param("isActive") boolean isActive);

    List<Proxy> findByCountry(@Param("country") String country);

    List<Proxy> findByIsActiveIsTrue();

}
package com.allproxies.ProxyManager.controller;

import com.allproxies.ProxyManager.exception.ProxyIdMismatchException;
import com.allproxies.ProxyManager.exception.ProxyNotFoundException;
import com.allproxies.ProxyManager.model.Proxy;
import com.allproxies.ProxyManager.repository.ProxyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/proxies")
public class ProxyController {

    @Autowired
    private ProxyRepository proxyRepository;

    @GetMapping
    public Iterable findAll() {
        return proxyRepository.findAll();
    }

    @GetMapping("/country/{proxyCountry}")
    public List findByCountry(@PathVariable String proxyCountry) {
        return proxyRepository.findByCountry(proxyCountry);
    }

    @GetMapping("/goodproxies")
    public List findByIsActiveIsTrue() {
        return proxyRepository.findByIsActiveIsTrue();
    }

    @GetMapping("/goodproxies/{proxyIsActive}")
    public List findByIsActive(@PathVariable boolean isActive) {
        return proxyRepository.findByIsActive(isActive);
    }

    @GetMapping("/{id}")
    public Proxy findOne(@PathVariable Long id) {
        Proxy proxy = proxyRepository.findOne(id);
        if (proxy == null)
            throw new ProxyNotFoundException("Not found proxy: " + id, new Throwable());
        return proxy;
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Proxy create(@RequestBody Proxy proxy) {
        return proxyRepository.save(proxy);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable Long id) {
        Proxy proxy = proxyRepository.findOne(id);
        if (proxy == null)
            throw new ProxyNotFoundException("Not found proxy: " + id, new Throwable());
        proxyRepository.delete(id);
    }

    @PutMapping("/{id}")
    public Proxy updateProxy(@RequestBody Proxy proxy, @PathVariable Long id) {
        if (proxy.getId() != id) {
            throw new ProxyIdMismatchException("Not found proxy: " + id);
        }
        Proxy old = proxyRepository.findOne(id);
        if (old == null) {
            throw new ProxyNotFoundException("Not found proxy: " + id, new Throwable());
        }
        return proxyRepository.save(proxy);
    }

}

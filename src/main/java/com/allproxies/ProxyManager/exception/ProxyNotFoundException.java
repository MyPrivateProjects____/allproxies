package com.allproxies.ProxyManager.exception;

public class ProxyNotFoundException extends RuntimeException {

    public ProxyNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

}
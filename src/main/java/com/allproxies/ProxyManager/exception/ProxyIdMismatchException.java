package com.allproxies.ProxyManager.exception;

import java.util.NoSuchElementException;

public class ProxyIdMismatchException extends NoSuchElementException {
    public ProxyIdMismatchException(String message) {
        super(message);
    }
}

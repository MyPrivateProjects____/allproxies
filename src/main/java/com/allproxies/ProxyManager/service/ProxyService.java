package com.allproxies.ProxyManager.service;



import com.allproxies.ProxyManager.model.Proxy;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
interface ProxyService {

//    Page<Proxy> getAllIsActiveProxies(Pageable pageable);

    public List<Proxy> findAll();

    public Proxy findProxy(Long id);

    public void save(Proxy proxy);

    public void delete(Long id);
}

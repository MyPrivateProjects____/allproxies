package com.allproxies.ProxyManager.service;

import com.allproxies.ProxyManager.model.Proxy;
import com.allproxies.ProxyManager.repository.ProxyRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class ProxyServiceImpl implements ProxyService {

    private final ProxyRepository proxyRepository;

    public ProxyServiceImpl(ProxyRepository proxyRepository) {
        this.proxyRepository = proxyRepository;
    }

    public List<Proxy> findAll() {
        List<Proxy> proxies = new ArrayList<>();
        for (Proxy proxy : proxyRepository.findAll()) {
            proxies.add(proxy);
        }
        return proxies;
    }

/*    @Override
    public Page<Proxy> getAllIsActiveProxies(Pageable pageable) {
        Page<Proxy> proxyList = proxyRepository.findByIsActiveIsTrueOrderByDateCreatedDesc(pageable);
        return proxyList;
    }*/



    public Proxy findProxy(Long id) {
        return proxyRepository.findOne(id);
    }

    public void save(Proxy proxy) {
        proxyRepository.save(proxy);
    }

    public void delete(Long id) {
        proxyRepository.delete(id);
    }
}

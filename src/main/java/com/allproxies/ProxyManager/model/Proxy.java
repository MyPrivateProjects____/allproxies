package com.allproxies.ProxyManager.model;

import javax.persistence.*;
import java.io.IOException;
import java.io.Serializable;
import java.net.InetSocketAddress;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import java.util.regex.Pattern;


import com.maxmind.geoip.Location;
import com.maxmind.geoip.LookupService;


@Entity(name = "proxy")
public class Proxy implements Serializable {

    @Transient
    public static final Pattern IP_PORT_PATTERN = Pattern.compile("^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5]):[0-9]+$");

    @Transient
    public static final Pattern IP_PATTERN = Pattern.compile("((1?\\d\\d?|2[0-4]\\d|25[0-5])\\.){3}(1?\\d\\d?|2[0-4]\\d|2\u200C\u200B5[0-5])+");

    @Transient
    public static Set<Proxy> PROXY = new HashSet<>();

    @Transient
    public static Set<Proxy> PROXYGOOD = new HashSet<>();

    @Transient
    public static Set<Proxy> PROXYBAD = new HashSet<>();

    @Transient
    public static Set<Proxy> PROXYVERIFIED = new HashSet<>();

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(name = "ip", nullable = false)
    private String ip;

    @Column(name = "port", nullable = false)
    private String port;

    @Column(name = "country")
    private String country;

    @Column(name = "city")
    private String city;

    //todo enums
   // @Enumerated(EnumType.STRING)
    @Column(name = "anonymity")
    private String anonymity;

    @Column(name = "ping")
    private Integer ping;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "date_created")
    private Date dateCreated;

    @Column(name = "is_active")
    private boolean isActive;

    @Column(name = "address")
    private InetSocketAddress address;


    public Proxy() {
    }

    public Proxy(String ip, String port) {
        this.ip = ip;
        this.port = port;
        this.dateCreated = new Date();
//        address = new InetSocketAddress(ip, Integer.parseInt(port));
        setCity();
        setCountry();
    }

    public Proxy(String ip, String port, Date date) {
        this.ip = ip;
        this.port = port;
        this.dateCreated = date;
        address = new InetSocketAddress(ip, Integer.parseInt(port));
        setCity();
        setCountry();
    }

    public Proxy(String ip, String port, String country, String city, String anonymity, int ping, Date dateCreated, boolean isactive) {
        this.ip = ip;
        this.port = port;
        this.anonymity = anonymity;
        this.ping = ping;
        this.dateCreated = dateCreated;
        this.isActive = isactive;
        setCity();
        setCountry();
    }

    public Proxy(String ipPort) {

        if (ipPort.contains(":")) {
            this.ip = ipPort.substring(0, ipPort.indexOf(":"));
            this.port = ipPort.substring(ipPort.indexOf(":") + 1, ipPort.length());
            if (!(Integer.parseInt(port) < 0 || Integer.parseInt(port) > 0xFFFF)) {
                address = new InetSocketAddress(ip, Integer.parseInt(port));
            } else {
                //Throw NotProxyException
            }
        } else {
            //Throw NotProxyException
        }
        setCity();
        setCountry();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry() {
        String geodata = getClass().getResource("/GeoIP.dat").getFile();
        LookupService cl = null;
        try {
            cl = new LookupService(geodata, LookupService.GEOIP_MEMORY_CACHE);
        } catch (IOException e) {
            e.printStackTrace();
        }
        String countrycode = cl.getCountry(ip).getCode();
        cl.close();

        this.country = countrycode;
    }

    public String getCity() {
        return city;
    }

    public void setCity() {
        String geocity = getClass().getResource("/GeoLiteCity.dat").getFile();
        LookupService cl = null;
        try {
            cl = new LookupService(geocity, LookupService.GEOIP_MEMORY_CACHE);
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println(ip);
        Location l1 = cl.getLocation(ip);
        String city;
        if (l1 != null) {
            city = l1.city;
        } else {
            city = "--";
        }
        cl.close();
        this.city = city;
    }

    public String getAnonymity() {
        return anonymity;
    }

    public void setAnonymity(String anonymity) {
        this.anonymity = anonymity;
    }

    public Integer getPing() {
        return ping;
    }

    public void setPing(Integer ping) {
        this.ping = ping;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    public InetSocketAddress getAddress() {
        return address;
    }

    public void setAddress(InetSocketAddress address) {
        this.address = address;
    }

    @Override
    public String toString() {
        return "Proxy{" +
                "id=" + id +
                ", ip='" + ip + '\'' +
                ", port='" + port + '\'' +
                ", country='" + country + '\'' +
                ", city='" + city + '\'' +
                ", anonymity='" + anonymity + '\'' +
                ", ping=" + ping +
                ", dateCreated=" + dateCreated +
                ", isActive=" + isActive +
                '}';
    }

    public String toStringIpPort() {
        return ip + ":" + port;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Proxy proxy = (Proxy) o;

        if (!getIp().equals(proxy.getIp())) return false;
        return getPort().equals(proxy.getPort());
    }

    @Override
    public int hashCode() {
        int result = getIp().hashCode();
        result = 31 * result + getPort().hashCode();
        return result;
    }
}
